(package-initialize)

;; override the default http with https
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")))

;; add melpa to the front
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)

(package-refresh-contents)
(package-install 'htmlize)
(package-install 'elixir-mode)

(require 'org)
(require 'ox)
(require 'ox-publish)

;; disable timestamps
(setq org-publish-use-timestamps-flag nil)
;; Allow #+bind: ...
(setq org-export-allow-bind-keywords t)

(defun kansi-org-publish-org-sitemap (title list)
  "Sitemap generation function."
  (concat "#+setupfile: ../org-templates/index.org\n\n"
          (org-list-to-generic list '(:istart "1. " :iend "\n\n"))
          ))

;; setup project for publishing
(setq org-publish-project-alist
      '(("org-blog"
         :base-directory "./blog/"
         :base-extension "org"
         :publishing-directory "./public/"
         :recursive t
         :publishing-function org-html-publish-to-html
         :headline-levels 2

         ;; used to generated link to Index page
         :html-link-home "/index.html"
         :html-link-up "index.html"

         ;; Generate index page
         :auto-sitemap t
         :sitemap-filename "index.org"
         :sitemap-title "Home"
         :sitemap-sort-files anti-chronologically
         :sitemap-function kansi-org-publish-org-sitemap
       )

        ("org-css"
         :base-directory "./blog/static"
         :base-extension "css"
         :publishing-directory "./public/css"
         :recursive t
         :publishing-function org-publish-attachment
         )

        ("org-static"
         :base-directory "./blog/static/"
         :base-extension  "svg\\|png"
         :publishing-directory "./public/images"
         :recursive t
         :publishing-function org-publish-attachment
         )

        ("org-images"
         :base-directory "./blog/"
         :base-extension  "png"
         :publishing-directory "./public"
         :recursive t
         :publishing-function org-publish-attachment
         )

        ("tib" :components ("org-blog" "org-css" "org-static" "org-images"))
        ))

(setq org-html-text-markup-alist
      '((bold . "<b>%s</b>")
        (code . "<code>%s</code>")
        (italic . "<i>%s</i>")
        (strike-through . "<del>%s</del>")
        (underline . "<span class=\"underline\">%s</span>")
        (verbatim . "<kbd>%s</kbd>")))


;; (add-to-list 'org-src-lang-modes '("plantuml" . plantuml))
;; (org-babel-do-load-languages 'org-babel-load-languages '((plantuml . t)))
;; (setq org-plantuml-jar-path (expand-file-name "/home/kansi/.local/bin/plantuml.jar"))

;; (defun sh ()
;;   (interactive)
;;   (ansi-term "/bin/zsh"))
